﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
    [SerializeField] private FPSCameraSwitchController m_FPSCamCtrl;
    [SerializeField] private UICameraControl m_UICameraControl;
    [SerializeField] private CameraMode m_CameraMode;
	// Use this for initialization
	void Awake () 
    {
        DontDestroyOnLoad(this);
        m_CameraMode = CameraMode.Normal;
    }

    private void Start()
    {
        m_FPSCamCtrl.Init();
        //m_FPSCamCtrl.SwitchToVNormalMode();
        //m_UICameraControl.SwitchToVNormalMode();
        m_FPSCamCtrl.SwitchToVRMode();
        m_UICameraControl.SwitchToVRMode();
    }
}
