﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCameraSwitchController : MonoBehaviour
{
    [SerializeField] private GyroControl m_GyroControl;
    [SerializeField] private Camera m_NormalCamera;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// 場合によってはRecenterの機能をするかも
    /// </summary>
    public void Init()
    {
        m_GyroControl.Init();
        SetMainCamera();
    }

    // TODO Init()だけで済ませるんだったらPrivateにすること。
    public void SetMainCamera()
    {
        if(m_NormalCamera == null)
        {
            m_NormalCamera = Camera.main;
        }
    }

    public void SwitchToVRMode()
    {
        m_NormalCamera.gameObject.SetActive(false);
        m_GyroControl.gameObject.SetActive(true);
    }

    public void SwitchToVNormalMode()
    {
        m_GyroControl.gameObject.SetActive(false);
        m_NormalCamera.gameObject.SetActive(true);
    }
}
