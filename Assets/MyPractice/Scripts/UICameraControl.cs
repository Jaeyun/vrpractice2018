﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraControl : MonoBehaviour
{
    [SerializeField] private GameObject m_UIVRCameraNormal;
    [SerializeField] private GameObject m_UIVRCameraVR;

    public void SwitchToVRMode()
    {
        m_UIVRCameraNormal.SetActive(false);
        m_UIVRCameraVR.SetActive(true);
    }

    public void SwitchToVNormalMode()
    {
        m_UIVRCameraVR.SetActive(false);
        m_UIVRCameraNormal.SetActive(true);
    }
}
