﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControl : MonoBehaviour
{
    private bool m_isNowUseGyro;
    private double lastCompassUpdateTime = 0;
    private Quaternion correction = Quaternion.identity;
    private Quaternion targetCorrection = Quaternion.identity;

    // Androidの場合はScreen.orientationに応じてrawVectorの軸を変換
    static Vector3 compassRawVector
    {
        get
        {
            Vector3 ret = Input.compass.rawVector;

            if (Application.platform == RuntimePlatform.Android)
            {
                switch (Screen.orientation)
                {
                    case ScreenOrientation.LandscapeLeft:
                        ret = new Vector3(-ret.y, ret.x, ret.z);
                        break;

                    case ScreenOrientation.LandscapeRight:
                        ret = new Vector3(ret.y, -ret.x, ret.z);
                        break;

                    case ScreenOrientation.PortraitUpsideDown:
                        ret = new Vector3(-ret.x, -ret.y, ret.z);
                        break;
                }
            }

            return ret;
        }
    }

    // Quaternionの各要素がNaNもしくはInfinityかどうかチェック
    static bool isNaN(Quaternion q)
    {
        bool ret = float.IsNaN(q.x) || float.IsNaN(q.y) || float.IsNaN(q.z) || float.IsNaN(q.w) ||
                        float.IsInfinity(q.x) || float.IsInfinity(q.y) || float.IsInfinity(q.z) || float.IsInfinity(q.w);
        return ret;
    }

    static Quaternion ChangeAxis(Quaternion q)
    {
        return new Quaternion(-q.x, -q.y, q.z, q.w);
    }

    bool IsNowUseGravity
    {
        get { return m_isNowUseGyro; }
    }

    void SetIsNowUseGravity(bool isON)
    {
        m_isNowUseGyro = isON;
    }

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Init()
    {
        Input.gyro.enabled = true;
        Input.compass.enabled = true;
    }

    private void Start()
    {
        m_isNowUseGyro = false;
    }

    void FixedUpdate()
    {
        // リアルタイムで座標軸を端末側に合わせて修正
        Quaternion gorientation = ChangeAxis(Input.gyro.attitude);

        // コンパスが変わった時間を基準として方角が変わったのかチェック
        if (Input.compass.timestamp > lastCompassUpdateTime)
        {
            lastCompassUpdateTime = Input.compass.timestamp;

            Vector3 gravity = Input.gyro.gravity.normalized;
            Vector3 rawvector = compassRawVector;
            Vector3 flatnorth = rawvector - Vector3.Dot(gravity, rawvector) * gravity;

            Quaternion corientation = ChangeAxis(Quaternion.Inverse(Quaternion.LookRotation(flatnorth, -gravity)));

            // +zを北にするためQuaternion.Euler(0,0,180)を入れる。
            Quaternion tcorrection = corientation * Quaternion.Inverse(gorientation) * Quaternion.Euler(0, 0, 180);

            // 計算結果が異常値になったらエラー
            // そうでない場合のみtargetCorrectionを更新する。
            if (!isNaN(tcorrection))
                targetCorrection = tcorrection;
        }

        if (Quaternion.Angle(correction, targetCorrection) < 45)
        {
            correction = Quaternion.Slerp(correction, targetCorrection, 0.02f);
        }
        else
        {
            correction = targetCorrection;
        }
        transform.localRotation = correction * gorientation;
    }
}
